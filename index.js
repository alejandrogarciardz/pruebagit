const persona = (nombre = 'sin nombre', edad = 0) => {
    return {
        'nombre' : nombre,
        'edad' : edad
    }
}

var pako = persona('Pako', 25)
console.log('Hola ', pako.nombre, 'tienes ', pako.edad, ' años')

document.querySelector('body h1').innerHTML = 'Hola ' + pako.nombre +' tienes ' + pako.edad+ ' años';
